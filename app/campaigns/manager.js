'use strict';

/**
 * Manager class
 */
class ManagerCampaigns {
    constructor(Campaign) {
        this.Campaign = Campaign;
        //this.statusValidator = statusValidator;
    }

    addNewCampaign(data) {
        console.log('data');
        console.log(data);
        return new Promise((resolve, reject) => {
            const newCampaign = new this.Campaign(data);
            newCampaign.save(function (err, result) {
                console.log(err);
                console.log(result);
                if (err) {
                    reject('An error occured while creating new message:' + err);
                }

                resolve(result);
            })
        });
    }

    deleteById(query) {

        //deleteById(query) {

        return new Promise((resolve, reject) => {
            this.Campaign.remove(query, (err, result) => {
                if (err) {
                    reject('An error occured while deleting messages: ' + err);
                }

                resolve(result);
            });
        });

        //
        // console.log('id ='+id);
        // return new Promise((resolve, reject) => {
        //     this.Message.remove({ campaign: id }, (err, result) => {
        //         if (err) {
        //             reject('An error occured while deleting message by ID="' + id + '", err:' + err);
        //         }
        //
        //         resolve(result);
        //     });
        // });
    }

    deleteMany(query) {
        return new Promise((resolve, reject) => {
            this.Campaign.remove(query, (err, result) => {
                if (err) {
                    reject('An error occured while deleting messages: ' + err);
                }

                resolve(result);
            });
        });
    }

    update(_campaign, data) {
        return new Promise((resolve, reject) => {
            // if (!this.statusValidator.isValid(status)) {
            //     reject('Invalid campaign: ' + _campaign);
            // } else {
            //     this.Campaign.findByIdAndUpdate(_id, { $set: { status: status } }, { new: true }, function (err, message) {
            //         if (err) reject(err);
            //
            //         resolve(message);
            //     });
            //}
            //
            // console.log(_campaign);
            // console.log(data);
            data.campaign=_campaign;

            const that=this;
            console.log('_campaign'+_campaign);


            // this.Campaign.findAndModify(
            //     {"campaign": _campaign},
            //     { "$inc": { "bill": 1 } },
            //     function(err,doc) {
            //         // work here
            //
            //     }
            // );

            this.Campaign.findOneAndUpdate({"campaign": _campaign}, data, {
                new: true,
                upsert: true // Make this update into an upsert
            },function (err, campaign) {
                if (err) reject(err);
                console.log("update");
                console.log(campaign);
                resolve(campaign);
            });
            /*
            this.Campaign.findAndModify
             this.Campaign.findOne({campaign: _campaign}, function (err, campaign) {

                    if (err) reject(err);
                        console.log('campaign'+campaign);
                            console.log(err);

                         if (campaign==null)
                         {

                             var newCampaign = new that.Campaign(data);
                             newCampaign.save(function (err, result) {

                                 console.log('result new');
                                 console.log(result);

                                 if (err) {
                                     reject('An error occured while creating new message:' + err);
                                 }


                                 resolve(result);
                             })

                         }else{
                             this.Campaign.update({campaign: _campaign}, data, { new: true }, function (err, campaign) {
                                 if (err) reject(err);
                                 console.log('campaign update ');
                                 console.log(campaign);
                                 resolve(campaign);
                             });
                         }

                 });
*/


        });
    }

    updateMultiple(items) {
        let toUpdate = [];

        for (let key in items) {
            toUpdate.push(this.update(items[key].campaign, items[key]));
        }

        return Promise.all(toUpdate);
    }

    //
    // updateMultipleStatuses(items) {
    //     let toUpdate = [];
    //     for (let key in items) {
    //         toUpdate.push(this.updateStatus(items[key]._id, items[key].status));
    //     }
    //
    //     return Promise.all(toUpdate);
    // }
}

module.exports = (campaignModel) => new ManagerCampaigns(campaignModel);