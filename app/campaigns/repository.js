'use strict';

/**
 * Repository class
 */
class Repository {
    constructor(Campaign) {
        this.Campaign = Campaign;
    }

    getList(query = {}) {
        return new Promise((resolve, reject) => {
            this.Campaign.find(query)
                .sort({createdAt: 1})
                .exec((err, res) => {
                    if (err) {
                        reject('An error occured fetching all messages, err:' + err)
                    }
                    
                    resolve(res);
                });
        });
    }
}

module.exports = (Campaign) => new Repository(Campaign);
