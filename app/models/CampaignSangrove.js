const mongoose = require('mongoose');

const CampaignSchema = mongoose.Schema({
    identifier: {
        type: String,
        required: true,
    },
    campaign: {
        type: Number,
        required: true,
    },
    event: {
        type: Number,
        required: true,
    },
    moq: {
        type: Number,
        required: true,
        default: 0
    },
    soldEvent: {
        type: String,
        required: false,
        default: 0
    },
    number_order: {
        type: String,
        required: false,
        default: 0
    },
    orders: {
        type: Number,
        required: true,
        default: 0
    },
    sold: {
        type: String,
        required: true,
        default: 0
    },
    status: {
        type: String,
        default: 1,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

module.exports = mongoose.model('CampaignSchema',CampaignSchema);